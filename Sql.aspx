﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

    <asp:Content runat="server" ContentPlaceHolderID="title">
       <h3>SQL DATA BASE</h3> 
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="concept">
    <p> Structured Query Language (SQL) is a specialized language for updating, deleting, and requesting information from databases.
        SQL is an ANSI and ISO standard, and is the de facto standard database query language.</p>
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="links">
       <p> Links </p> 
    <a href="https://www.w3schools.com/sql/default.asp"> SQL Tutorial </a><br>
        </asp:Content>
     <asp:Content runat="server" ContentPlaceHolderID="example">
    <h3> Syntax </h3>
        <img src="/Images/sql.png" alt="sql" width="350">
    <p></p>
        <p> The following SQL statement selects all fields from "Customers" where country is "Germany" AND city is "Berlin" </p>
        </asp:Content>